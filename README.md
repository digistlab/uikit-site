**Ini merupakan repo clone dari https://github.com/uikit/uikit-site**

Ini merupakan repo latihan untuk memahami:
- Pemahaman dasar repositori versioning control Git
- Clone repositori dari internet ke local repo
- Clone repositori menjadi project di PHPStorm
- Implemenrasi HTML + CSS + Library UIKit
- Implementasi VueJS
- Pemahaman dasar build manager NodeJS dan NPM
- Pemahaman dasar Yarn
- Pemahanan dasar dependency library builder
---

## Persiapan

Untuk latihan, pastikan di komputer Anda telah terinstal:

1. Webserver Apache/Nginx
2. Membuat virtual host dengan nama "uikit-site.localhost". Silakan baca di https://www.cloudways.com/blog/configure-virtual-host-on-windows-10-for-wordpress
3. Periksa konfirgurasi virtual host dengan mengakses melalui browser
3. NodeJS
4. Yarn

---
##Clone dan build project
1. Clone project ini melalui PHPStorm. Atau dapat melalui menu VCS -> Checkout from version control -> Git. Dan masukkan URL repositori ini.
2. Pada terminal (CMD) ketikkan perintah:
`yarn` 
3. Ketikkan perintah
`yarn setup`
4. Setelah proses selesai ketikan alamat url uikit-site.localhost pada browser